# Import necessary libraries
import numpy as np
import pandas as pd
from shapely.geometry import Polygon, Point, LineString
import ezdxf
import datetime
import random
from math import cos, sin, radians
from scipy.spatial import ConvexHull

# Define the names of the dxf files that are used as orders
shapes = ['Flat pattern - test6.DXF', 
          'Flat pattern - test9.DXF',
          'Flat pattern - test10.DXF']

# Define the name of the dxf file that is used as high-runner
high_runners = ['Flat pattern - test7.DXF',]


# Define function for writing DXF to shapes
def read_dxf(n): 
    
    # Read DXF files
    dxf = []
    for row in n:
       filename = row
       dxf.append(ezdxf.readfile(filename))
    
    # Draw entities from the DXF files
    shapes = {}
    for i, row in enumerate(dxf):
        modelspace = row.modelspace()
        name = "shape " + str(i+1)
        shapes[name] = []
        for e in modelspace:
            if e.dxftype() == 'LINE':
                shapes[name].append([e.dxf.start[0], e.dxf.start[1]])
                shapes[name].append([e.dxf.end[0], e.dxf.end[1]])
            elif e.dxftype() == 'CIRCLE':
                C = Point([e.dxf.center[0], e.dxf.center[1]]).buffer(e.dxf.radius)
                x = C.exterior.xy[0]
                y = C.exterior.xy[1]
                for i in range(len(x)):
                    shapes[name].append([x[i],y[i]])               
            elif e.dxftype() == 'ARC':
                if (e.dxf.start_angle > 180) and (e.dxf.end_angle < e.dxf.start_angle):
                    theta = np.radians(np.linspace(e.dxf.start_angle, 360 + e.dxf.end_angle, 100))
                    x = e.dxf.center[0] + e.dxf.radius * np.cos(theta)
                    y = e.dxf.center[1] + e.dxf.radius * np.sin(theta)
                    LS = LineString(np.column_stack([x, y]))
                    for i in LS.coords:
                        shapes[name].append([i[0], i[1]])
                else:
                    theta = np.radians(np.linspace(e.dxf.start_angle, e.dxf.end_angle, 100))
                    x = e.dxf.center[0] + e.dxf.radius * np.cos(theta)
                    y = e.dxf.center[1] + e.dxf.radius * np.sin(theta)
                    LS = LineString(np.column_stack([x, y]))
                    for i in LS.coords:
                        shapes[name].append([i[0], i[1]])
    
    # Collect all the vertices of the shapes
    pts = []
    for name in shapes:
        pts.append(np.array(list(shapes[name])))
    
    # Find a convex hull for the different shapes
    hulls = []
    for i in range(len(pts)):
        hull_indices = ConvexHull(pts[i]).vertices
        hull_pts = pts[i][hull_indices, :]
        hulls.append(hull_pts)
    
    # Find vertices of the convex hull
    shapes = []
    for col in hulls:
        X = []
        Y = []
        for row in col:
            X.append(row[0])
            Y.append(row[1])
        minX = abs(min(X))
        minY = abs(min(Y))
        X = [x + minX for x in X]
        Y = [y + minY for y in Y]
        Points = list(zip(X, Y))
        shapes.append(Points)
    return shapes

# Add all vertices of the different shapes to list
shapes = read_dxf(shapes)

# Add all vertices of the high-runner shape to list
high_runners = read_dxf(high_runners)

# Generate random integers between 1 and 100 indicating the number of orders for each shape
def Num_orders(x):
    num = []
    for i in range(len(x)):
        num.append(random.randint(1, 100))
    return num
num = Num_orders(shapes)

# Initialize an empty order list
order_list = []

# Add the ordered shapes to the order list
for i in range(len(shapes)):
    for j in range(num[i]):
        order_list.append(shapes[i])

# Apply different scalings to the shapes and add to order list again
shapes = []
for shape in order_list:
    scale = [random.randint(5, 10),random.randint(5, 10)]
    shapes.append(Polygon([p[0]*scale[0], p[1]*scale[1]] for p in shape))
order_list = pd.DataFrame({'geometry': shapes})

# Functions used for finding reference points for each shapes
def get_indexes_min_value(x):
    min_value = min(x)
    if x.count(min_value) > 1:
        return [i for i, l in enumerate(x) if l == min(x)]
    else:
        return [x.index(min(x))]
def ref(x):
    ref = []
    for i, shape in x.iterrows():
        x, y = shape['geometry'].exterior.coords.xy
        index = get_indexes_min_value(x)
        if len(index) > 1:
            y = [y[idx] for idx in index]
            ref.append([min(x), max(y)])
        else:
            ref.append([min(x), y[index[0]]])
    return ref 

# Function for calculation of shapes' area (bounding box)
def area(x):
    area = []
    for i, shape in x.iterrows():
        bounds = shape['geometry'].bounds
        area.append((bounds[2]-bounds[0])*(bounds[3]-bounds[1]))
    return area

# Function for applying thickness to the shapes
t = [8, 12, 15, 25, 40, 50]
def thickness(x):
    thickness = []
    for i in range(len(x)):
        thickness.append(random.choice(t))
    return thickness

# Function for generation of random due dates
def generate_dates(x):
    dates = []
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(30)
    for i in range(0,len(x)):
        time_between_dates = end_date - start_date
        days_between_dates = time_between_dates.days
        random_number_of_days = random.randrange(days_between_dates)
        dates.append(start_date + datetime.timedelta(days=random_number_of_days))
    return dates

# Add reference points of the shapes
order_list['reference'] = ref(order_list)    

# Add calculation of the area of the shapes
order_list['area'] = area(order_list)

# Add the thickness of the different shapes
order_list['thickness'] = thickness(order_list)

# Add random due date to the shapes
order_list['due'] = generate_dates(order_list)

# Manual definition of the sheets on stock
sheet1 = {'name': 'sheet1',
          'length': 6000,
          'width': 2500,
          'thickness': 8,
          'price': 7600}

sheet2 = {'name': 'sheet2',
          'length': 6000,
          'width': 3350,
          'thickness': 12,
          'price': 1880}

sheet3 = {'name': 'sheet3',
          'length': 6000,
          'width': 2500,
          'thickness': 15,
          'price': 4238}

sheet4 = {'name': 'sheet4',
          'length': 6000,
          'width': 2500,
          'thickness': 25,
          'price': 3301}

sheet5 = {'name': 'sheet5',
          'length': 4000,
          'width': 2000,
          'thickness': 40,
          'price': 4037}

sheet6 = {'name': 'sheet6',
          'length': 4000,
          'width': 2000,
          'thickness': 50,
          'price': 26935}

# Initialize stock with the defined sheets
stock = [sheet1, sheet2, sheet3, sheet4, sheet5, sheet6]

# Decide which cutting machine to use for the sheet depending on the thickness
for sheet in stock:
    if 8 <= sheet['thickness'] <= 12:
        sheet['machine'] = 'laser'
    elif 15 <= sheet['thickness'] <= 25:
        sheet['machine'] = 'plasma'   
    elif 40 <= sheet['thickness'] <= 50:
        sheet['machine'] = 'flame'

# Normalization of prices
prices = []     
for sheet in stock:
    prices.append(sheet['price'])
prices_norm = [float(i)/30000 for i in prices]

# Apply cut-off thresholds for the different sheets with respect to their price
cut_off = []
for i in prices_norm:
    diff = 100-50
    co = 50 + diff*i
    if co > 100:
        co = 100.0
    cut_off.append(co)
for i in range(len(stock)):
    stock[i]['cut-off'] = cut_off[i]

# Add high-runner parts to the different sheet on stock
# Different scalings are added to the shapes of high-runners 
high_runner = []
for i, sheet in enumerate(stock):
    scale = [random.randint(5, 10),random.randint(5, 10)]
    high_runner.append(pd.DataFrame({'geometry': [Polygon([p[0]*scale[0], p[1]*scale[1]] for p in high_runners[0])]}))
    high_runner[i]['reference'] = ref(high_runner[i])
    high_runner[i]['area'] = area(high_runner[i])
    high_runner[i]['consumption'] = random.uniform(0,1)
for i, sheet in enumerate(stock):
    sheet['high-runner'] = high_runner[i]

# Sorting the dates and areas in the dataframe (Earliest due date first, then largest area)
order_list = order_list.sort_values(['due', 'area'], ascending=[True, False])
