This repository contains the Python scripts and dxf files that are used in connection with a VT2 project with the theme "Development of Manufacturing Systems" at Aalborg University.

The script in Scheduling.py is the scheduling algorithm that can take in orders from the fictitious order system in Orders.py

The directory "Validation shapes" contains the shapes that were used for performing the validation of the scheduling algorithm.

The directory "Basic_nesting_shapes" contains the four basic shapes that the order system is based on.
