# Import necessary libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from shapely import affinity
import itertools
import time
import datetime
from scipy.spatial import ConvexHull
from nest2D import Point, Box, Item, nest, SVGWriter
from svgpathtools import svg2paths, wsvg 

# Import stock and list of orders from ERP
from Orders_new import order_list, stock

# Begin time measurement
start = time.time()

def nearest(items, pivot):
    due = min(items, key=lambda x: abs(x - pivot))
    return due 

# Defining information on the cutting machines
# Cutting speeds [m/s]
v_cut = {'plasma': 0.032, 'laser': 0.020, 'flame': 0.005}  

# Cost of running cutting machine [DKK/s]
c_cut = {'plasma': 0.105, 'laser': 0.105, 'flame': 0.105}

# Time for changing a sheet [s]
t_change = {'plasma': 600, 'laser': 600, 'flame': 1800}

nests = []

for sheet in stock:
    box = Box(sheet["width"]*100000, sheet["length"]*100000)
    orders = []
    for i, order in order_list.iterrows():
        if order["thickness"] == sheet["thickness"]:
            orders.append(order)
            
    print("Evaluating configurations for {}".format(sheet["name"]))
    for L in range(1, len(orders) + 1):
        for subset in itertools.combinations(orders, L):
            items = []
            sheet['due'] = nearest([shape["due"] for shape in subset], datetime.date.today())
            for shape in subset:
                items.append(shape["item"])

            pgrp = nest(items, box)
            
            if len(pgrp) != 1:
                print("Nest is not feasible")
                break
            
            sw = SVGWriter()
            sw.write_packgroup(pgrp, int(sheet['width']/10), int(sheet['length']/10))

            sw.save()
    
            paths, attributes = svg2paths('out.svg')

            points = []
            for path in paths:
                for line in path:
                    points.append([line[0].real, line[0].imag])

            points = np.array(points)

            hull = ConvexHull(points)

            area = hull.volume * 100

            cost = 0

            mu = area/(sheet['width']*sheet['length'])
            print("Material usage: " + str(round(mu*100,1)) + " %")

            # Calculate the material cost
            out = round(sheet['price']-(1-mu)*sheet['price']*0.15,1)
        
            # Add material cost to the total cost
            cost += out 

            #print('Cost function 1 (Material Usage): ' + str(out))
                
            ## f2: Sheet change
        
            # Calculate cost of sheet change
            out = 2*0.05*t_change[sheet['machine']]

            cost += out
    
            #print('Cost function 2 (Sheet change): ' + str(out))


            # Decide if sheet should be used for high-runners or be put back to stock
            if mu*100 >= stock[0]['cut-off']:
                
                #print('Cut to stock')

                ## f3: Cutting to stock
            
                # Define penalty term
                pen = 2
    
                # Calculate remaining area on the sheet
                remaining_area = sheet['length']*sheet['width']*(1-mu)
    
                # Calculate the number of high-runners
                high_run = int(remaining_area/sheet['high-runner']['area'])
    
                #print('No. of high-runners: ' + str(high_run))

                # Calculate cost for nesting high-runners
                out = round(sheet['price'] + (high_run/sheet['high-runner']['consumption'][0])**pen,1)
    
                # Add high-runner cost to total cost
                cost += out

                #print('Cost function 3 (Cutting to Stock): ' + str(out))
    
            else:
                pass
                #print('Surplus to stock')


            ## f4: Cutting cost

            # Calculating the total circumferences of all of the parts that are nested 
            C = 0
            for shape in subset:
                C += shape["geometry"].length

            # Calculate the cost for cutting
            out = round(((c_cut[sheet['machine']]/v_cut[sheet['machine']]*(C/1000)/10000)), 1)

            # Add cutting cost to the total cost
            cost += out 

            #print('Cost function 4 (Cost for Cutting): ' + str(out))
            
            ## f5: Due date
    
            # Calculate due date costma
            out = round(((sheet['due']-datetime.date.today()).days)*10000, 1)

            # Add due date cost to total cost
            cost += out
    
            #print('Cost function 5 (Due Date): ' + str(out))


            nests.append([sheet["name"], subset, cost])


min_cost = np.min([p[2] for p in nests])
index = [p[2] for p in nests].index(min_cost)
optimal_nest = nests[index]


for sheet in stock:
    if sheet["name"] == optimal_nest[0]:
        box = Box(sheet["width"]*100000, sheet["length"]*100000)
        
items = []
for shape in optimal_nest[1]:
    items.append(shape["item"])

pgrp = nest(items, box)
        
sw = SVGWriter()
sw.write_packgroup(pgrp, int(sheet['width']/10), int(sheet['length']/10))

sw.save()
    
paths, attributes = svg2paths('out.svg')

points = []
for path in paths:
    for line in path:
        points.append([line[0].real, line[0].imag])

points = np.array(points)

hull = ConvexHull(points)
paths, attributes = svg2paths('out.svg')

plt.plot(points[:, 0], points[:, 1], 'o')
for simplex in hull.simplices:
    plt.plot(points[simplex, 0], points[simplex, 1], 'k-')
plt.title("Optimal Nest on {}".format(optimal_nest[0]))
plt.show()

print("")
print("{} nests performed in {} seconds".format(len(nests), round(time.time()-start, 1)))
print("")
print("The optimal nest is found on {} with a total cost of {}".format(optimal_nest[0], 
                                                                 optimal_nest[2]))

