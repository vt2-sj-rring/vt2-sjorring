# Import necessary libraries
import pygad
import numpy as np
import matplotlib.pyplot as plt
from shapely import affinity
import itertools
import time
import datetime
from scipy.spatial import ConvexHull
import pandas as pd

# Import stock and list of orders from ERP
from Orders import stock, order_list

# Assign the shapes from the order list to fitting sheets
for sheet in stock:
    count = 0
    area = 0
    parts = []
    for i, part in order_list.iterrows(): 
        diff = 0.75*sheet['length']*sheet['width'] - area
        if part['area'] <= diff and sheet['thickness'] == part['thickness']:
            count += 1
            part['name'] = "shape" + str(count)
            parts.append(part)
            order_list.drop(i, inplace=True)
            area += part['area']
    parts = pd.DataFrame(parts).reset_index(drop=True)
    # shift column 'name' to first position
    first_column = parts.pop('name')
    # insert column using insert(position,column_name,first_column) function
    parts.insert(0, 'name', first_column)
    sheet['shapes'] = parts

# Apply the nearest due date for the shapes as due date for the sheets
def nearest(items, pivot):
    due = min(items, key=lambda x: abs(x - pivot))
    return due 
for sheet in stock:
    sheet['due'] = nearest(sheet['shapes']['due'], datetime.date.today())

# Defining information on the cutting machines
# Cutting speeds [m/s]
v_cut = {'plasma': 0.032, 'laser': 0.020, 'flame': 0.005}  

# Cost of running cutting machine [DKK/s]
c_cut = {'plasma': 0.105, 'laser': 0.105, 'flame': 0.105}

# Time for changing a sheet [s]
t_change = {'plasma': 600, 'laser': 600, 'flame': 1800}

# Begin time measurement
start = time.time()

# Defining the scheduling function
def f(s):
    # Print the name of the sheet
    print('Sheet name: ' + str(stock[s]['name']))
    
    # Print the number of parts from the order system
    print('Number of ordered parts: ' + str(len(stock[s]['shapes'])))
    
    # Initial cost before nesting
    cost = 0
    
    ## Cost 1: Material usage
    
    # Resolution of the dotted-board (mm between dots)
    resolution = 50
    
    # Definition of the dots on the dotted board
    x = np.linspace(0, stock[s]['length'], int(stock[s]['length'] / resolution)+1)
    y = np.linspace(0, stock[s]['width'], int(stock[s]['width'] / resolution)+1)
    X, Y = np.meshgrid(x, y)
    x = []
    y = []
    for i in range(0, X.shape[1]):
        for j in range(len(Y)):
            x.append(X[j, i])
            y.append(Y[j, i])
    pts = np.column_stack([x, y])

    ## Defintion of the inner-fit dots for the shapes that are nested
    
    # Initiate a list for defining the gene space
    gene_space = []
    
    # Loop through the parts on the sheet
    for i, shape in stock[s]['shapes'].iterrows():
        inlist = []
        for j, point in enumerate(pts):
            a1, a2 = np.array(shape['reference']), np.array(point)
            v = a2 - a1        
            transform = affinity.translate(shape['geometry'], v[0], v[1])
            X, Y = transform.exterior.coords.xy
            if max(X) <= pts[-1][0] and min(Y) >= pts[0][1] and max(Y) <= pts[-1][1]:
                inlist.append(j)
        # Append inner-fit dots for parts to gene space
        gene_space.append(inlist)
        
    # Defining fitness function for the nesting
    def fitness_func(solution, solution_idx):
        
        # Dimension of the chromosomes (equal to the number of shapes)
        dim = len(gene_space)
        
        # Initiate arrays for storing information on the nests
        L = np.empty(shape=(len(gene_space),1))
        W = np.empty(shape=(len(gene_space),1))
        poly = np.empty(shape=(len(gene_space),1),dtype='object')

        for i in range(0, dim):
            a1, a2 = np.array(stock[s]['shapes']['reference'][i]), np.array(pts[solution[i]])
            v = a2 - a1
            transform = affinity.translate(stock[s]['shapes']['geometry'][i], v[0], v[1])
            L[i,0] = max(transform.exterior.coords.xy[0])
            W[i,0] = max(transform.exterior.coords.xy[1])
            poly[i,0] = transform
        
        # Calculate the product of the sums of the lenghts and widths
        output = np.sum(L)*np.sum(W)
         
        # Checking if any of the shapes are overlapping 
        # (If overlap --> penalty of 10^9)
        for pair in itertools.combinations(range(0, dim), 2):
            if poly[pair[0],0].intersects(poly[pair[1],0]):
                output += 10**9
        
        # Calculation of fitness score for the given solution
        fitness = 1.0 / output
        return fitness

    ## Preparing of the parameters to use in PYGAD optimization
    
    # Assign fitness function
    fitness_function = fitness_func

    # Number of generations that the algorithm runs   
    num_generations = 500

    # Number of parents that are selected for mating
    num_parents_mating = 2
    
    # Number of chromosomes per generation
    sol_per_pop = int(3*len(gene_space))

    # Number of genes per chromosome
    num_genes = len(gene_space)
    
    # Crossover operation type
    crossover_type = "single_point"
    
    # Mutation operation type
    mutation_type = "adaptive"
    
    # Specify number of genes to mutate (for low- and high-quality solutions)
    mutation_num_genes = (int(len(gene_space)/2), 1)

    # Preparing an instance of the genetic algorithm
    ga_instance = pygad.GA(fitness_func=fitness_function,
                           num_generations=num_generations,
                           num_parents_mating=num_parents_mating,
                           sol_per_pop=sol_per_pop,
                           num_genes=num_genes,
                           crossover_type=crossover_type,
                           mutation_type=mutation_type,
                           mutation_num_genes=mutation_num_genes,
                           gene_space=gene_space,
                           gene_type=int)

    # Start the optimation
    ga_instance.run()
    ga_instance.plot_result()
    
    # Store information about the best solution found by the algorithm
    solution, solution_fitness, solution_idx = ga_instance.best_solution()
    
    # Initiate list for storing points for calculation of convex hull  
    points = []
    
    # Plotting the nests for the different sheets
    fig, ax = plt.subplots()
    
    for i, shape in stock[s]['shapes'].iterrows():
        d = int(solution[i])
        a1, a2 = np.array(shape['reference']), np.array(pts[d])
        v = a2 - a1
        transform = affinity.translate(shape['geometry'], v[0], v[1])
        X, Y = transform.exterior.coords.xy
        points.extend(list(transform.exterior.coords))
        ax.fill(X, Y, color=(21/256,48/256,102/256), zorder=2)
        ax.plot(x, y, 'o', color=(230/256, 230/256, 230/256), zorder=1)
        centroid = transform.centroid.xy
        ax.text(centroid[0][0]-80, centroid[1][0], str(i+1), fontsize = 10, color = "white")
    fig.suptitle("Nesting on " + str(stock[s]['name']) + " to be cut at " + str(stock[s]['due']))
    
    # Find the convex hull containing all points of the nested parts
    points = np.array(points)
    hull = ConvexHull(points)
    for simplex in hull.simplices:
        ax.plot(points[simplex, 0], points[simplex, 1], color=(179/256,18/256,18/256))
    
    # Show the nest
    plt.show()
    
    # Write nested shapes to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write(stock[s]['shapes'][['name', 'area', 'thickness', 'due']].to_string())
    tfile.close()    
    
    # Calculate the area of the convex hull of the nested parts
    area = hull.volume
    
    # Calculate the material usage of the nested parts
    mu = area/(stock[s]['width']*stock[s]['length'])
    print("Material usage: " + str(round(mu*100,1)) + " %")
    
    # Calculate the material cost
    out = round(stock[s]['price']-(1-mu)*stock[s]['price']*0.15,1)
    
    # Add material cost to the total cost
    cost += out 
    
    print('Cost function 1 (Material Usage): ' + str(out))
    
    # Write cost for material usage to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write
    tfile.write('\n')
    tfile.write('\n')
    tfile.write('Cost function 1 (Material Usage): ' + str(out))
    tfile.close()
    
    ## f2: Sheet change
        
    # Calculate cost of sheet change
    out = 2*0.05*t_change[stock[s]['machine']]
    
    cost += out
    
    print('Cost function 2 (Sheet change): ' + str(out))
    
    # Write cost of sheet change to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write
    tfile.write('\n')
    tfile.write('Cost function 2 (Sheet change): ' + str(out))
    tfile.close()
    
    # Decide if sheet should be used for high-runners or be put back to stock
    if mu*100 >= stock[s]['cut-off']:
        
        print('Cut to stock')
    
        ## f3: Cutting to stock
        
        # Define penalty term
        pen = 2
        
        # Calculate remaining area on the sheet
        remaining_area = stock[s]['length']*stock[s]['width']*(1-mu)
        
        # Calculate the number of high-runners
        high_run = int(remaining_area/stock[s]['high-runner']['area'])
        
        print('No. of high-runners: ' + str(high_run))
    
        # Calculate cost for nesting high-runners
        out = round(stock[s]['price'] + (high_run/stock[s]['high-runner']['consumption'][0])**pen,1)
        
        # Add high-runner cost to total cost
        cost += out
    
        print('Cost function 3 (Cutting to Stock): ' + str(out))
        
        # Write cost of cutting to stock to log
        tfile = open(stock[s]['name'] + '_log.txt', 'a')
        tfile.write
        tfile.write('\n')
        tfile.write('Cost function 3 (Cutting to Stock): ' + str(out))
        tfile.close()
        
    else:
        
        print('Surplus to stock')
    
    ## f4: Cutting cost
    
    # Calculating the total circumferences of all of the parts that are nested 
    C = 0
    for part in stock[s]['shapes']['geometry']:
        C += part.length
    
    # Calculate the cost for cutting
    out = round(c_cut[stock[s]['machine']]/v_cut[stock[s]['machine']]*(C/1000),1)
    
    # Add cutting cost to the total cost
    cost += out 
    
    print('Cost function 4 (Cost for Cutting): ' + str(out))
    
    # Write cost for cutting to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write
    tfile.write('\n')
    tfile.write('Cost function 4 (Cost for Cutting): ' + str(out))
    tfile.close()
    
    ## f5: Due date
    
    # Calculate due date cost
    out = round(((stock[s]['due']-datetime.date.today()).days)*10000, 1)
    
    # Add due date cost to total cost
    cost += out
    
    print('Cost function 5 (Due Date): ' + str(out))
    
    # Write cost of due date to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write
    tfile.write('\n')
    tfile.write('Cost function 5 (Due Date): ' + str(out))
    tfile.close()
    
    # Print the total cost for the nest
    print('Total cost: ' + str(round(cost,1)))
    print('\n')
    
    # Write total cost and material usage to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write
    tfile.write('\n')
    tfile.write('Total cost: ' + str(round(cost,1)))
    tfile.write('\n')
    tfile.write('\n')
    tfile.write("Material usage: " + str(round(mu*100,1)) + " %")
    tfile.close()
    
    return stock[s]['name'], stock[s]['machine'], round(cost,1)
    
# Initiate array to store information from the nesting
nest = np.empty(shape=(len(stock),3),dtype='object')

# Run the algorithm for each sheet on stock
for s in range(0,len(stock)):
    start = time.time()
    sheet, machine, cost = f(s)
    # Add sheet name, cutting machine and cost to array
    nest[s] = ([sheet, machine, cost])
    end = time.time()
    duration = round((end - start), 1)
    print("Computational time: " + str(duration) + " seconds")
    print('\n')
    # Write duration to log
    tfile = open(stock[s]['name'] + '_log.txt', 'a')
    tfile.write('\n')
    tfile.write('\n')
    tfile.write("Computational time: " + str(duration) + " seconds")
    tfile.close()

# Sort array to get the lowest cost first
nest_sorted = nest[np.argsort(nest[:, 2])]

print("Priority list:"'\n' + str(nest_sorted))

# Assign sheets with the lowest costs to their machines
laser = nest_sorted[np.where(nest_sorted[:,1] == 'laser')][0]
plasma = nest_sorted[np.where(nest_sorted[:,1] == 'plasma')][0]
flame = nest_sorted[np.where(nest_sorted[:,1] == 'flame')][0]

print('\n'"Next sheet to the laser cutter is " + str(laser[0]) + ' with a cost of ' + str(laser[2]))
print("Next sheet to the plasma cutter is " + str(plasma[0]) + ' with a cost of ' + str(plasma[2]))
print("Next sheet to the flame cutter is " + str(flame[0]) + ' with a cost of ' + str(flame[2]))


